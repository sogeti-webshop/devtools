#!/bin/bash

cd ./../ProductService/ && sh ./../ProductService/build-and-publish.sh && cd ./../Devtools/ &
cd ./../AuthenticationService/ && sh ./../AuthenticationService/build-and-publish.sh && cd ./../Devtools/ &
wait
