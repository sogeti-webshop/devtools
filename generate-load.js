var http = require('http');

if (process.argv.length < 6) {
    console.log('4 arguments need to be provided, which represents the api host, api port, product api path and number of requests')
    console.log('example: node generate-load.js "127.0.0.1" "8080" "/products" "1000"')
    process.exit()
}

var host = process.argv[2]
var port = process.argv[3]
var numberOfRequests = parseInt(process.argv[5])
var path = process.argv[4]

var requestOptions = {
    host: host,
    port: port,
    path: path,
    method: 'GET',
    headers: {
	'Content-Type': 'application/json'
    }
}

console.time(`execution time with ${numberOfRequests} items`); 

var result = new Array(numberOfRequests)

for (var i = 0; i < numberOfRequests; i++) {
    result[i] = sendRequest()
}

Promise.all(result).then((data) => console.timeEnd(`execution time with ${numberOfRequests} items`))

function sendRequest() {
    return new Promise((resolve, reject) => {
	http.get(requestOptions, function(res) {
	    res.setEncoding('utf8');
	    res.on('data', function(data) {
		resolve(data);
	    });
	    res.on('error', () => {
		reject("Something went wrong");
	    })
	});
    })
}    
