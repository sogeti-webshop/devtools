# Devtools

This repository contains everything ops-related. It contains a docker-compose script that allows you to instantly setup the entire application without hassle.

## Het runnen van de applicatie

Voordat de applicatie gerunt kan worden zal Docker geïnstalleerd moeten worden. Instructies voor het installeren van Docker voor verschillende platformen zijn hier te vinden: https://docs.docker.com/engine/installation/.

NOTE: Het is belangrijk dat je toegang hebt tot de Gitlab docker registry. Bart van der Wal en ik hebben beide admin rechten en kunnen meerdere mensen toevoegen voor het draaien van de applicatie.

Om de docker images te pullen moet er ingelogd worden bij de docker registry door het volgende commando in de terminal uit te voeren: `docker login registry.gitlab.com`

Om de applicatie moet alleen nog twee dns records aan je locale DNS toegevoegd te worden. Op `linux` en `OSX` kan de /etc/hosts file aangepast worden en de volgende twee dingen toevoegen:

127.0.0.1   api
127.0.0.1   orderservice

Het is belangrijk dat de hosts die voor de naam api en orderservice staat verwijst naar de docker host!

Op een Windows machine kunnen dezelfde records worden toegevoegd, maar in een andere file. Dit staat beschreven op deze website https://support.rackspace.com/how-to/modify-your-hosts-file/.

Vervolgens kan er naar de devtools folder worden genavigeert en docker-compose up -d worden gedraaid. Dit start de gehele applicatie op welke bereikbaar is op ${DOCKER_HOST}:8080

## Sonarqube

The sonarqube directory contains a docker-image and a jar file that adds [scoverage](http://scoverage.org) support. The dockerfile can be found [here](https://hub.docker.com/r/mauropalsgraaf/sonarqube-with-scoverage/)

## docker-compose

the docker-compose up allows you to setup the entire application without problems. Make sure you have the correct access to the docker-registry and that you logged in using `docker login`. When this is done correctly, just run `docker-compose up` to run all the containers.

## CD/CI

Running `build-local-libraries.sh` will build the product- and authenticationservice and publish them according to the setting to their respective build.sbt
Running `build-and-publish.sh` will build all the services and create docker-images, which will be published to all the respective repositories if all tests succeed

## Tools

To setup the tools locally for this project like sonarqube, you can enter the tools directory which contains a docker-compose. Running this docker-compose file will start all tools for this project, in this case only sonarqube. Sonarqube will have a volume mapping which will make sure that data won't be gone whenever a new container will be created.

