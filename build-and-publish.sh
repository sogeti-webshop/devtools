#!/bin/bash

cd ./../Api/ && sh ./../Api/build-and-publish.sh && cd ./../Devtools/ &
cd ./../OrderService/ && sh ./../OrderService/build-and-publish.sh && cd ./../Devtools/ &
cd ./../Web/ && sh ./../Web/build-and-publish.sh && cd ./../Devtools/ &
wait
